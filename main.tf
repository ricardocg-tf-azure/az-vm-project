terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.20.0"
    }
  }
}

provider "azurerm" {
    features {}
    subscription_id = "50505e05-cd97-4b8d-a080-40b5b880ce44"
    tenant_id = "34f55fb5-3a46-4626-b706-5d94d061b0d4"
}

module "myterraformgroup" {
    source   = "git::https://gitlab.com/ricardocg-tf-azure/az-resource-group.git?ref=master"
    name     = "myResourceGroup"
    location = "eastus"
    env      = "test"
}

module "myVnet" {
    source          = "git::https://gitlab.com/ricardocg-tf-azure/az-virtual-network.git?ref=master"
    name            = "myVnet"
    address         = ["10.0.0.0/16"]
    location        = "eastus"
    rg_name         = module.myterraformgroup.name
    env             = "test"
}

module "subnets" {
    source              = "git::https://gitlab.com/ricardocg-tf-azure/az-subnets.git?ref=master"
    subnets             = var.subnets
    name                = ["SubTest1"]
    rg                  = module.myterraformgroup.name
    vnet                = module.myVnet.name
    address_prefixes    = ["10.0.3.0/26"]
}

#Create public IP (make it module)
resource "azurerm_public_ip" "myterraformpublicip" {
    name                         = "myPublicIP"
    location                     = "eastus"
    resource_group_name          = module.myterraformgroup.name
    allocation_method            = "Dynamic"

    tags = {
        environment = "Terraform Demo"
    }
}

#Create security group (make it module)
resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "myNetworkSecurityGroup"
    location            = "eastus"
    resource_group_name = module.myterraformgroup.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Demo"
    }
}

resource "azurerm_network_interface" "myterraformnic" {
    #count                       = var.subnets
    name                        = "myNIC"
    location                    = "eastus"
    resource_group_name         = module.myterraformgroup.name

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = module.subnets.subnets.id[0] #make it variable
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.myterraformpublicip.id
    }

    tags = {
        environment = "Terraform Demo"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "example" {
    network_interface_id      = azurerm_network_interface.myterraformnic.id
    network_security_group_id = azurerm_network_security_group.myterraformnsg.id
}

#Generate random name for security group
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = module.myterraformgroup.name
    }

    byte_length = 8
}

module "mySa" {
    source              = "git::https://gitlab.com/ricardocg-tf-azure/az-storage-account.git?ref=master"
    name                = "diag${random_id.randomId.hex}"
    rg                  = module.myterraformgroup.name
    location            = "eastus"
    env                 = "test"
}

#Create VM 

resource "tls_private_key" "example_ssh" {
  algorithm = "RSA"
  rsa_bits = 4096
}

output "tls_private_key" { value = tls_private_key.example_ssh.private_key_pem }

resource "azurerm_linux_virtual_machine" "myterraformvm" {
    name                  = "myVM"
    location              = "eastus"
    resource_group_name   = module.myterraformgroup.name
    network_interface_ids = [azurerm_network_interface.myterraformnic.id]
    size                  = "Standard_DS1_v2"

    os_disk {
        name              = "myOsDisk"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "myvm"
    admin_username = "azureuser"
    disable_password_authentication = true

    admin_ssh_key {
        username       = "azureuser"
        public_key     = tls_private_key.example_ssh.public_key_openssh
    }

    boot_diagnostics {
        storage_account_uri = module.mySa.primary_blob_endpoint
    }

    tags = {
        environment = "Terraform Demo"
    }
}