variable "subnets" {
  default = 1
  description = "Number of subnets"
}